import {
    FETCH_PEOPLE_SUCCESS,
    FETCH_PEOPLE_ERROR,
    FILTER_PEOPLE_SUCCESS,
    DELETE_PEOPLE_SUCCESS,
  } from "../constants";
  
  /**
   *  PEOPLE
   */
  
  export const setPeople = (payload) => ({ type: FETCH_PEOPLE_SUCCESS, payload });
  export const setPeopleError = (payload) => ({
    type: FETCH_PEOPLE_ERROR,
    payload,
  });
  export const setFilter = (payload) => ({
    type: FILTER_PEOPLE_SUCCESS,
    payload,
  });
  export const deletePeople = (payload) => ({
    type: DELETE_PEOPLE_SUCCESS,
    payload,
  });
  
  