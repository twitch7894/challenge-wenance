/**
 * API SWAPI
 */

 const HOST = "https://swapi.dev/api";

 export const ENDPOINTS = {
   PEOPLE: `${HOST}/people/`
 };
 
 