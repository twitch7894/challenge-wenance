import { useEffect, useCallback, useState, useMemo } from "react";
import axios from "axios";
import { ENDPOINTS } from "../config/endpoints";
import { useSelector, useDispatch } from "react-redux";
import { setPeople, setPeopleError, setFilter } from "../actions";
import CardsList from "../components/card/cardsList";
import SearchForm from "../components/search/searchForm";
import Layout from "../components/layout/layout";
import Loading from "../components/loading/loading";
import MessageError from "../components/errors/messageError";

function App() {
  const [search, setSearch] = useState("");

  const people = useSelector((state) => state.people);
  const { loading, data, error, resultFilter, infoError } = people;

  const dispatch = useDispatch();

  const FetchDataPeople = useCallback(async () => {
    try {
      const response = await axios.get(ENDPOINTS.PEOPLE);
      dispatch(setPeople(response.data));
    } catch (err) {
      dispatch(setPeopleError(err.message));
    }
  }, [dispatch]);

  useEffect(() => {
    FetchDataPeople();
  }, [FetchDataPeople]);

  const searchFilter = useMemo(() => {
    if (loading) return;
    return data.filter((e) =>
      e.name.toLowerCase().includes(search.toLowerCase())
    );
  }, [search, data, loading]);

  const removePeople = (created) => {
    return {
      resultFilter: resultFilter.filter((e) => e.created !== created),
      data: data.filter((e) => e.created !== created),
    };
  };

  return (
    <Layout>
      <SearchForm
        setSearch={setSearch}
        dispatch={dispatch}
        searchFilter={searchFilter}
        setFilter={setFilter}
      />
      {!loading ? (
        <CardsList data={resultFilter} removePeople={removePeople} />
      ) : (
        <Loading />
      )}
      {error && <MessageError infoError={infoError} />}
    </Layout>
  );
}

export default App;
