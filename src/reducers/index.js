import people from "./people";
import { combineReducers } from "redux";

export default combineReducers({
  people
});

