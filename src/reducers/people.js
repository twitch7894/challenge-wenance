import {
  FETCH_PEOPLE_INIT,
  FETCH_PEOPLE_SUCCESS,
  FETCH_PEOPLE_ERROR,
  FILTER_PEOPLE_SUCCESS,
  DELETE_PEOPLE_SUCCESS,
} from "../constants";

const initialState = {
  data: [],
  loading: true,
  error: false,
  infoError: null,
  resultFilter: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PEOPLE_INIT:
      return initialState;
    case FETCH_PEOPLE_SUCCESS:
      return {
        ...state,
        data: action.payload.results,
        resultFilter: action.payload.results,
        loading: false,
        error: false,
      };
    case FETCH_PEOPLE_ERROR: {
      return {
        ...state,
        loading: true,
        error: true,
        infoError: action.payload,
      };
    }
    case FILTER_PEOPLE_SUCCESS: {
      return {
        ...state,
        resultFilter: action.payload,
      };
    }
    case DELETE_PEOPLE_SUCCESS: {
      return {
        ...state,
        resultFilter: action.payload.resultFilter,
        data: action.payload.data
      };
    }
    default:
      return state;
  }
};

export default reducer;