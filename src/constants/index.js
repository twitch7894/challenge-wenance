/**
 *  GET PEOPLE
 */

 export const FETCH_PEOPLE_INIT = "FETCH_PEOPLE_INIT";
 export const FETCH_PEOPLE_SUCCESS = "FETCH_PEOPLE_SUCCESS";
 export const FETCH_PEOPLE_ERROR = "FETCH_PEOPLE_ERROR";
 export const FILTER_PEOPLE_SUCCESS = "FILTER_PEOPLE_SUCCESS";
 export const DELETE_PEOPLE_SUCCESS = "DELETE_PEOPLE_SUCCESS";
 export const EMPTY_PEOPLE = "No result found";
 
 