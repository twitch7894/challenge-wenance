export default function Loading() {
    return (
      <div className="loading">
        <i className="fad fa-spinner-third load"></i>
      </div>
    );
  }
  
  