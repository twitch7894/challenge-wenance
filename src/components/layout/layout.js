export default function Layout({ children }) {
    return <div className="layout" id="container">{children}</div>;
};
  
  