import Card from "./card";
import { EMPTY_PEOPLE } from "../../constants";

export default function CardsList({ data, removePeople }) {
  const empty = (text) => data.length === 0 && text;

  return (
    <div className="cardlist">
      {data.map((e) => (
        <Card
          key={e.created}
          data={e}
          removePeople={removePeople}
        />
      ))}
      <p className="cardlist-text">{empty(EMPTY_PEOPLE)}</p>
    </div>
  );
};

