import { deletePeople } from "../../actions";
import { useDispatch } from "react-redux";

export default function Card({ data, removePeople }) {
  const dispatch = useDispatch();

  return (
    <div className="card" key={data.created}>
      <div className="card-text-container">
        <p className="card-text">{data.name}</p>
        <p className="card-text">Height: {data.height}</p>
        <p className="card-text">Gender: {data.gender}</p>
      </div>
      <div className="card-button-container">
        <button
          className="card-button"
          onClick={() => dispatch(deletePeople(removePeople(data.created)))}
        >
          Delete
        </button>
      </div>
    </div>
  );
}

