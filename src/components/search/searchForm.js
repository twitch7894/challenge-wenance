export default function SearchForm({
  setSearch,
  dispatch,
  searchFilter,
  setFilter,
}) {
  const hanleKeypress = (key) => {
    if (key === "Enter") dispatch(setFilter(searchFilter));
  };
  return (
    <div className="search-container">
      <input
        className="search_input"
        type="text"
        onKeyPress={(e) => hanleKeypress(e.key)}
        onChange={(e) => setSearch(e.target.value)}
      />
      <button
        className="search_button"
        id="button_search"
        onClick={() => dispatch(setFilter(searchFilter))}
      >
        <i className="fas fa-search"></i>
      </button>
    </div>
  );
};