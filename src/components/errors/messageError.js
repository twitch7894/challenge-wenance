export default function MessageError({ infoError  }) {
    return <div className="error">{infoError}</div>
}