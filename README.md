# Challenge Wenance  🚀

## Installation 📋

requires [Node.js](https://nodejs.org/) v14+ to run.

Install the dependencies and devDependencies and start the server.

```sh
cd challenge-wenance
npm i
npm start
```

## Test with cypress

Requirement must be running the project on port 3000
```sh
npm start
```
Running cypress
```sh
npm test
```


# Project Image

## Click on homepage.js

<a style="text-align:center">
    <img src=" https://i.ibb.co/yg6MhSj/Captura-de-Pantalla-2021-05-09-a-la-s-19-19-54.png">
</a>

